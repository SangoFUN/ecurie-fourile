        <?php
         if(session_status() == PHP_SESSION_NONE) {
  session_start();
  }
  if($_SESSION['type'] == 0) {
    header('Location: ./');
}
            require_once("src/Bootstrap.php");
            require_once("src/Calendar/Month.php");
            require_once("src/Calendar/Events.php");
            $pdo = get_pdo();
            $events = new \Calendar\Events(get_pdo());
            try {
            $month = new Calendar\Month($_GET['month'] ?? null, $_GET['year'] ?? null); 
            }
            catch(\Exception $e) {
              $month = new Calendar\Month(); 
            }
            $start = $month->getStartingDay();
            $start = $start->format('N') === '1' ? $start : $month->getStartingDay()->modify('last monday');
            $weeks= $month->getWeeks();
            $end = (clone $start)->modify('+' . (6 + 7 * ($weeks - 1)) . 'days');
            $events = $events->getEventsBetweenByDay($start,$end);
            require_once("views/header.php");
        ?>
    <div class="calendar content">

    <div class="d-flex flex-ow align-items-center justify-content-between mx-sm-3">
    <h1><?= $month->toString(); ?></h1>
        <?php if(isset($_GET['success'])): ?>
    <div class="container"> 
        <div class="alert alert-success">
            L'évènement a bien été enregistré
        </div>
    </div>
        <?php endif; ?>
        <div>
            <a href="planning_cours_admin.php?month=<?= $month->previousMonth()->month; ?>&year=<?= $month->previousMonth()->year; ?>" class="btn btn-primary">&lt;</a>
            <a href="planning_cours_admin.php?month=<?= $month->nextMonth()->month; ?>&year=<?= $month->nextMonth()->year; ?>" class="btn btn-primary">&gt;</a>
        </div>
    </div>

    <?php $month->getWeeks(); ?>

    <table class="calendar__table calendar__table--<?= $weeks; ?>weeks ">
            <?php for ($i = 0; $i < $weeks; $i++): ?>
                <tr>
                    <?php foreach($month->days as $k => $day):
                        $date = (clone $start)->modify("+" . ($k + $i * 7) . "days");
                        $eventsForDay = $events[$date->format('Y-m-d')] ?? [];
                        $isToday = date('Y-m-d') === $date->format('Y-m-d');
                        ?>
                    <td class="<?= $month->withinMonth($date) ? '' : 'calendar__othermonth'; ?> <?= $isToday ? 'is-today' : ''; ?>">
                        <<?php if($i === 0): ?>div class="calendar__weekday"><?php endif ?><?= $day ?> </div>
                        <a class="calendar__day" href="add.php?date=<?=$date->format('Y-m-d');?>"> <?= $date->format('d'); ?></a>
                        <?php foreach($eventsForDay as $event): ?>
                        <div class="calendar__event">
                            <?= (new DateTime($event['start']))->format('H:i') ?>  <a href="edit.php?id=<?= $event['id'];?>"><?= h($event['name']); ?></a>
                        <?php endforeach ?>
                        
                    </td>
                    <?php endforeach; ?>
                </tr>
                <?php endfor; ?>
        </table>

        <a href="add.php" class="calendar__button">+</a>
    </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
    <script src="assets/js/Simple-Slider.js"></script>
        <?php require_once("footer.php"); ?>