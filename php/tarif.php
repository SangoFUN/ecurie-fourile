<?php 
if(session_status() == PHP_SESSION_NONE) {
    session_start();
    }
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Les Tarifs</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abril+Fatface">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alfa+Slab+One">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora">
    <link rel="stylesheet" href="assets/fonts/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/Article-Clean.css">
    <link rel="stylesheet" href="assets/css/Article-List.css">
    <link rel="stylesheet" href="assets/css/Contact-Form-Clean.css">
    <link rel="stylesheet" href="assets/css/Features-Boxed.css">
    <link rel="stylesheet" href="assets/css/Footer-Dark.css">
    <link rel="stylesheet" href="assets/css/Highlight-Blue.css">
    <link rel="stylesheet" href="assets/css/Highlight-Clean.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css">
    <link rel="stylesheet" href="assets/css/Login-Form-Clean.css">
    <link rel="stylesheet" href="assets/css/Login-Form-Dark.css">
    <link rel="stylesheet" href="assets/css/Projects-Horizontal.css">
    <link rel="stylesheet" href="assets/css/Registration-Form-with-Photo.css">
    <link rel="stylesheet" href="assets/css/Simple-Slider.css">
    <link rel="stylesheet" href="assets/css/Team-Boxed.css">
    <link rel="stylesheet" href="assets/css/Navigation-with-Button.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body>
    <?php require_once('menu.php'); ?>
      
    <div class="container content">
        <div class="row">
            <div class="col-md-12" style="margin-top: 5%;margin-bottom: 5%;">
                <div class="table-responsive">
                    <h1>Tarifs :</h1>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Licence</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Pour personne mineur : 25€</td>
                            </tr>
                            <tr>
                                <td>Pour personne majeur : 36€</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5%;">
                <div class="table-responsive">
                    <h2>Tarifs propriétaires : </h2>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Cours</th>
                                <th>Carte</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Cours passagers collectifs : 12€</td>
                                <td>10 leçons : 100€</td>
                            </tr>
                            <tr>
                                <td>Cours semi particulier (3 personnes max) : 18 € par personnes</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Cours particuliers : 25€</td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5%;">
                <div class="table-responsive">
                    <h2>Tarifs Pensions :</h2>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Pensions :</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Pensions boxe (2 repas de foin, 3 repas de granulés) : 300€</td>
                            </tr>
                            <tr>
                                <td>Pension herbe (seulement l'été) : 180€</td>
                            </tr>
                            <tr>
                                <td>Pension hôtel : 20€ par jour</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5%;">
                <div class="table-responsive">
                    <h2>Sorties paddock / marcheur :</h2>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Sorties</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Forfait 3 fois par semaines : 60€</td>
                            </tr>
                            <tr>
                                <td>Occasionnel : 8€</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="row">
            <div class="col-md-12" style="margin-bottom: 5%;">
                <div class="table-responsive">
                    <h2>Travail du cheval :</h2>
                    <table class="table">
                        <thead>
                            <tr>
                                <th>Forfait</th>
                                <th>Occasionnel</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Forfait 2 fois par semaines : 120€</td>
                                <td>Travail occasionnel du cheval : 17€</td>
                            </tr>
                            <tr>
                                <td>Forfait 3 fois par semaines : 165€</td>
                                <td></td>
                            </tr>
                            <tr>
                                <td>Forfait 4 fois par semaines : 200€</td>
                                <td></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
    <script src="assets/js/Simple-Slider.js"></script>

    <?php require_once('footer.php'); ?>
