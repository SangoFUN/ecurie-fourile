pln = document.getElementById("planning");

var request = new XMLHttpRequest();
request.open("GET", "assets/js/planning.json", false);
request.setRequestHeader('Cache-Control', 'no-cache');
request.send(null);
var my_JSON_object = JSON.parse(request.responseText);


for (i = 0; i < 7; i++) {
	for (j = 0; j < my_JSON_object[i]["hour"].length; j++) {
		pln.rows[(parseInt(my_JSON_object[i]["hour"][j]) - 8) + 1].cells[i + 1].innerHTML = my_JSON_object[i]["text"][j];
		pln.rows[(parseInt(my_JSON_object[i]["hour"][j]) - 8) + 1].cells[i + 1].style.background = my_JSON_object[i]["color"][j]
	}
}
var dlt = document.getElementById('delete');
var textinput = document.getElementById('text');
var colorinput = document.getElementById('color');
dlt.addEventListener('change', function() {
	textinput.hidden = dlt.checked;
	colorinput.hidden = dlt.checked;	
})




