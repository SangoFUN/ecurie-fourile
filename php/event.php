<?php
if(session_status() == PHP_SESSION_NONE) {
    session_start();
    }
    require_once("src/Bootstrap.php");
    require_once("src/Calendar/Events.php");
    $pdo = get_pdo();
    $events = new \Calendar\Events(get_pdo());
    if (!isset($_GET['id'])) {
        header('Location: error404');
    }
    try {
        $event = $events->find($_GET['id']);
    } catch(\Exception $e) {
        e404();
    }
    render("header", ['title' => $event->getName()]);
    ?>
    <div class="container content">
    <p class="event">Information sur l'évènement :  <b><?= h($event->getName()); ?></b></p><br><br>
    <ul class="">
        <li class="description">Date: <?= $event->getStart()->format('d/m/Y'); ?></li><br>
        <li class="description">Heure de démarrage: <?= $event->getStart()->format('H:i'); ?></li><br>
        <li class="description">Heure de fin: <?= $event->getEnd()->format('H:i'); ?></li><br>
        <li class="description">
        <strong>Description:</strong><br> 
        <?= h($event->getDescription());?>
        <?php if($event->getDescription()== null) {
            echo "Il n'y a pas de description pour cette évènement !";
        }
        ?></li>
    </ul>

    <div class="row">
        <div class="col-md-12"></div>

    </div>
</div>

    <?php require_once("footer.php"); ?>