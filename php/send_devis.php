<?php
 if(session_status() == PHP_SESSION_NONE) {
  session_start();
  }

  $secret = "6LdPdZkbAAAAAMujWnQ29Pgcy_EEcsW4I1wT3ysK";
$response = $_POST['g-recaptcha-response'];
$remoteip = $_SERVER['REMOTE_ADDR'];
 
$api_url = "https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$response."&remoteip=".$remoteip;
 
$decode = json_decode(file_get_contents($api_url), true);
 
if ($decode['success'] == true) {
    echo 'humain';
} else {
    echo 'pas humain';
}

?>

!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Demande de devis</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abril+Fatface">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alfa+Slab+One">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora">
    <link rel="stylesheet" href="assets/fonts/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/Article-Clean.css">
    <link rel="stylesheet" href="assets/css/Article-List.css">
    <link rel="stylesheet" href="assets/css/Contact-Form-Clean.css">
    <link rel="stylesheet" href="assets/css/Features-Boxed.css">
    <link rel="stylesheet" href="assets/css/Footer-Dark.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css">
    <link rel="stylesheet" href="assets/css/Projects-Horizontal.css">
    <link rel="stylesheet" href="assets/css/Registration-Form-with-Photo.css">
    <link rel="stylesheet" href="assets/css/Simple-Slider.css">
    <link rel="stylesheet" href="assets/css/Team-Boxed.css">
    <link rel="stylesheet" href="assets/css/Navigation-with-Button.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>

</head>

<body>
  <?php require_once("menu.php"); ?>
 
    <section class="register-photo">
    <h1 class="title_user content"> Demande de Devis : </h1>
        <div class="form-container">
            <div class="image-holder"></div>
            <form method="post" action="send_devis_success.php">
                <h2 class="text-center"><strong>Demander</strong> un devis</h2>
                <div class="form-group"><input class="form-control" type="text" name="society" placeholder="Société" required></div>
                <div class="form-group"><input class="form-control" type="text" name="last_name" placeholder="Nom" required></div>
                <div class="form-group"><input class="form-control" type="text" name="first_name" placeholder="Prénom" required></div>
                <div class="form-group"><input class="form-control" type="int" name="phone_number" placeholder="Numéro de téléphone" required></div>
                <div class="form-group"><input class="form-control" type="email" name="email" placeholder="Email" required></div>
                <div class="form-group"><input class="form-control" type="text" name="address" placeholder="Adresse" required></div>
                <div class="form-group"><input class="form-control" type="int" name="postal_code" placeholder="Code Postal" required></div>
                <div class="form-group"><input class="form-control" type="text" name="town" placeholder="Ville" required></div>
                <div class="form-group"><input class="form-control" type="text" name="country" placeholder="Pays" required></div>
                <div class="form-group"><input class="form-control" type="text" name="product" placeholder="Produit" required></div>
                <div class="form-group"><button class="btn btn-primary btn-block" data-sitekey="6LdPdZkbAAAAACysnMa0-UIghwloYrIoIQbW1NjE" data-callback='onSubmit' type="submit">Envoyer</button></div>
            </form>
        </div>
    </section>
</body>
</html>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/bs-init.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
    <script src="assets/js/Simple-Slider.js"></script>

    <?php require_once("footer.php"); ?>

