<?php 
 if(session_status() == PHP_SESSION_NONE) {
    session_start();
    }

 
$secret = "6LdPdZkbAAAAAMujWnQ29Pgcy_EEcsW4I1wT3ysK";
$response = $_POST['g-recaptcha-response'];
$remoteip = $_SERVER['REMOTE_ADDR'];
 
$api_url = "https://www.google.com/recaptcha/api/siteverify?secret=".$secret."&response=".$response."&remoteip=".$remoteip;
 
$decode = json_decode(file_get_contents($api_url), true);
 
if ($decode['success'] == true) {
    echo 'humain';
} else {
    echo 'pas humain';
}


?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    
    <title>Formulaire de contact</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abril+Fatface">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alfa+Slab+One">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/Article-Clean.css">
    <link rel="stylesheet" href="assets/css/Article-List.css">
    <link rel="stylesheet" href="assets/css/Contact-Form-Clean.css">
    <link rel="stylesheet" href="assets/css/Features-Boxed.css">
    <link rel="stylesheet" href="assets/css/Footer-Dark.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css">
    <link rel="stylesheet" href="assets/css/Projects-Horizontal.css">
    <link rel="stylesheet" href="assets/css/Simple-Slider.css">
    <link rel="stylesheet" href="assets/css/Team-Boxed.css">
    <link rel="stylesheet" href="assets/css/Navigation-with-Button.css">
    <link rel="stylesheet" href="assets/css/style.css">
    	
<script src="https://www.google.com/recaptcha/api.js" async defer></script>

</head>

<body>

<script>
  function onSubmit(token) {
    document.getElementById("contact").submit();
  }
</script>

  <?php require_once("menu.php"); ?>
      
    <section class="contact-clean content" style="height: 750px; padding-top: 10%;">
        <form method="post" id="contact" action="send.php">
            <h2 class="text-center">Contactez nous</h2>
            <div class="form-group"><input class="form-control" type="text" name="name" placeholder="Nom" required></div>
            <div class="form-group"><input class="form-control" type="email" name="email" placeholder="Email" required></div>
            <div class="form-group"><textarea class="form-control" name="message" placeholder="Message" rows="14" minlength="50" maxlength="500" required></textarea></div>

            <div class="form-group"><button class="btn btn-primary btn-block" data-sitekey="6LdPdZkbAAAAACysnMa0-UIghwloYrIoIQbW1NjE" data-callback='onSubmit' type="submit">Envoyer</button></div>
        </form>
    </section>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/bs-init.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
    <script src="assets/js/Simple-Slider.js"></script>

    <?php require_once("footer.php"); ?>

