<?php
    if(session_status() == PHP_SESSION_NONE) {
        session_start();
        } 
    if($_SESSION['type'] == 0) {
        header('Location: ./');
    }
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Page d'administration</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abril+Fatface">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alfa+Slab+One">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/Features-Boxed.css">
    <link rel="stylesheet" href="assets/css/Footer-Dark.css">
    <link rel="stylesheet" href="assets/css/Login-Form-Clean.css">
    <link rel="stylesheet" href="assets/css/Login-Form-Dark.css">
    <link rel="stylesheet" href="assets/css/Map-Clean.css">
    <link rel="stylesheet" href="assets/css/Navigation-with-Button.css">
    <link rel="stylesheet" href="assets/css/Simple-Slider.css">
    <link rel="stylesheet" href="assets/css/Team-Boxed.css">
    <link rel="stylesheet" href="assets/css/Navigation-with-Button.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body>
    <?php require_once("menu.php"); ?>
      
    
        <div class="container content"> 
            <div class="row">
                <div class="col-md-4">
                    <section class="login-dark" style="border-top-left-radius: 6px;border-top-right-radius: 6px;border-bottom-right-radius: 6px;border-bottom-left-radius: 6px;background: transparent;">
                        <form  style="border-color: rgb(52,58,64);background: #282d32;">
                            <h2 class="sr-only"></h2>
                            <div class="illustration"><i class="icon ion-edit" style="border-color: rgb(255,255,255);color: rgb(255,255,255);"></i></div>
                            <div><a class="btn btn-primary btn-block" href="register.php" style="background: rgb(255,255,255);color: rgb(170,170,170);">Création d'un compte</a></div>
                            <div><a class="btn btn-primary btn-block" href="register_horse.php" style="color: rgb(170,170,170);background: rgb(255,255,255);">Ajout d'un cheval à vendre</a></div>
                            <div><a class="btn btn-primary btn-block" href="register_partner.php" style="background: rgb(255,255,255);color: rgb(170,170,170);">Ajout d'un partenaire</a></div><a class="btn btn-primary btn-block" href="register_activity.php" style="background: rgb(255,255,255);color: rgb(170,170,170);">Ajout d'une activité</a><a class="btn btn-primary btn-block" href="register_gallery.php" style="background: rgb(255,255,255);color: rgb(170,170,170);">Ajouter une image dans la galerie</a><a class="btn btn-primary btn-block" href="register_future_events.php" style="background: rgb(255,255,255);color: rgb(170,170,170);">Ajouter un événement à venir</a>
                        </form>
                    </section>
                </div>
                <div class="col-md-4">
                    <section class="login-dark" style="border-top-left-radius: 6px;border-top-right-radius: 6px;border-bottom-right-radius: 6px;border-bottom-left-radius: 6px;background: transparent;">
                        <form style="background: #282d32;">
                            <h2 class="sr-only">Login Form</h2>
                            <div class="illustration"><i class="fa fa-edit" style="color: rgb(255,255,255);"></i></div>
                            <div class="form-group"><a class="btn btn-primary btn-block" href="change_maps.php" style="background: rgb(255,255,255);color: rgb(170,170,170);">Modifier Google Maps</a></div>
                            <div class="form-group"><a class="btn btn-primary btn-block" href="change_facebook.php" style="background: rgb(255,255,255);color: rgb(170,170,170);">Modifier la page Facebook</a></div>
                        </form>
                    </section>
                </div>
                <div class="col-md-4">
                    <section class="login-dark" style="background: transparent;color: var(--pink);border-top-left-radius: 6px;border-top-right-radius: 6px;border-bottom-right-radius: 6px;border-bottom-left-radius: 6px;border-color: #282d32;">
                        <form method="post" style="background: #282d32;">
                            <h2 class="sr-only">Login Form</h2>
                            <div class="illustration"><i class="icon ion-ios-list" style="color: rgb(255,255,255);"></i></div>
                            <div class="form-group"><a class="btn btn-primary btn-block" href="profiles.php" style="background: rgb(255,255,255);color: rgb(170,170,170);">Liste des comptes</a></div>
                            <div class="form-group"><a class="btn btn-primary btn-block" href="horse.php" style="background: rgb(255,255,255);color: rgb(170,170,170);">Liste des chevaux à vendre</a></div>
                            <div class="form-group"><a class="btn btn-primary btn-block" href="partner.php" style="background: rgb(255,255,255);color: rgb(170,170,170);">Liste des partenaires</a></div><a class="btn btn-primary btn-block" href="activity.php" style="background: rgb(255,255,255);color: rgb(170,170,170);">Liste des activités</a><a class="btn btn-primary btn-block" href="gallery.php" style="background: rgb(255,255,255);color: rgb(170,170,170);">La galerie du club</a><a class="btn btn-primary btn-block" href="future_events.php" style="background: rgb(255,255,255);color: rgb(170,170,170);">Liste des événements à venir</a>
                        </form>
                    </section>
                </div>
            </div>
        </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
    <script src="assets/js/Simple-Slider.js"></script>
    <?php
                                $servername_footer = "cohrmxwbetty.mysql.db";
                                $username_footer = "cohrmxwbetty";
                                $password_footer = "7Pz9KYmziT7KfBo";
                                $database_footer = "cohrmxwbetty";
                                
                                $conn_footer = new mysqli($servername_footer, $username_footer, $password_footer, $database_footer);
                                
                                if ($conn_footer->connect_error) {
                                    die('<p style="color:red">'."Database connection failed: " . $conn_footer->connect_error);
                                }
                                    $sql_maps = "SELECT * FROM maps";
                                    $sql_fb = "SELECT * FROM facebook";
                                    
                                    $result_maps = $conn_footer->query($sql_maps);
                                    $result_fb = $conn_footer->query($sql_fb);
                                    $data_maps = $result_maps->fetch_assoc();
                                    $data_fb = $result_fb->fetch_assoc();
                                        ?>
<footer class="footer-dark">
        <div class="container" style="min-width:100%;">
            <div class="row">
                <div class="col-sm-6 col-md-3 col-xl-5 item">
                    <section class="map-clean">
                        <div class="container">
                            <div class="intro">
                                <h2 class="text-center">Notre écurie :</h2>
                                <p class="text-center" style="color:#FFF;opacity:0.6;">L'adresse actuelle de notre écurie :</p>
                            </div>
                        </div>
                        <div style="margin-bottom:15%">
                        <?='<iframe allowfullscreen="" frameborder="0" src="https://www.google.com/maps/embed/v1/place?key=' . $data_maps['key_maps'] . '&amp;q=' . $data_maps['number'] . '+' . $data_maps['street'] . '%2C+' . $data_maps['postal_code'] . '+' . $data_maps['town'] . '&amp;zoom=15" width="100%" height="501"></iframe>';?>
                        </div>
                    </section>
 
                </div>
                <div class="col-sm-6 col-md-3 item" >
                    <div class="reseausociaux">
                            <div style="margin-right:30%;">
                            <h2 class="text-center">Page Facebook :</h2>
                            <p class="text-center" style="opacity:0.6;">Pour suivre l'actualité :</p>
                            </div>
                        <div id="fb-root" ></div>
                        <?= '<script async defer crossorigin="anonymous" src="https://connect.facebook.net/fr_FR/sdk.js#xfbml=1&version=v10.0&appId=' . $data_fb['appId'] . '&autoLogAppEvents=1" nonce="dGsQrTiJ"></script>'; ?>
                        <div class="fb">
                            <?= '<div class="fb-page" data-href="https://www.facebook.com/' . $data_fb['name_url'] . '" data-tabs="timeline,messages" data-width="" data-height="" data-small-header="false" data-adapt-container-width="true" data-hide-cover="false" data-show-facepile="true">'; ?>
                                <?= '<blockquote cite="https://www.facebook.com/' . $data_fb['name_url'] . '" class="fb-xfbml-parse-ignore">'; ?>
                                    <?= '<a href="https://www.facebook.com/' . $data_fb['name_url'] . '">' . $data_fb['name_page'] . '</a>'; ?>
                                </blockquote>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-xl-4 item text cent">
                    <h3 class="centh3">Ecuries FOURILE</h3>
                    <p>Club et Ecuries de propriétaires familiales et conviviale située à wadelincourt (08200).</p>
                </div>
            </div>
            <p class="copyright">Ecuries FOURILE © 2021</p>
        </div>
    </footer>
