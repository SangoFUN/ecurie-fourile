<?php
 if(session_status() == PHP_SESSION_NONE) {
  session_start();
  }
if($_SESSION['type'] == 1) {

}
else {
  header('Location: ./');
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Ajout d'un partenaire</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abril+Fatface">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alfa+Slab+One">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora">
    <link rel="stylesheet" href="assets/fonts/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/Article-Clean.css">
    <link rel="stylesheet" href="assets/css/Article-List.css">
    <link rel="stylesheet" href="assets/css/Contact-Form-Clean.css">
    <link rel="stylesheet" href="assets/css/Features-Boxed.css">
    <link rel="stylesheet" href="assets/css/Footer-Dark.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css">
    <link rel="stylesheet" href="assets/css/Projects-Horizontal.css">
    <link rel="stylesheet" href="assets/css/Registration-Form-with-Photo.css">
    <link rel="stylesheet" href="assets/css/Simple-Slider.css">
    <link rel="stylesheet" href="assets/css/Team-Boxed.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/Navigation-with-Button.css">
</head>

<body>
  <?php require_once("menu.php"); ?>
      
    <section class="register-photo">
    <h1 class="title_user content"> Ajouter un Partenaire : </h1>
        <div class="form-container">
            <div class="image-holder"></div>
            <form method="post" action="validate_register_partner.php" enctype='multipart/form-data'>
            <h2 class="text-center"><strong>Ajoute</strong>  un nouveau partenaire </h2>
                <div class="form-group"><input class="form-control" type="text" name="name" placeholder="Nom" required></div>
                <div class="form-group"><input class="form-control" type="text" name="description" placeholder="Description du partenaire"required></div>
                <div class="form-group"><input class="form-control" type="text" name="link" placeholder="Lien vers le site internet"required></div>
                <input type='file' name='file' />

                <div class="form-group"><button class="btn btn-primary btn-block" name='but_upload'  type="submit">Valider l'ajout du partenaire</button></div>
            </form>
        </div>
    </section>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/bs-init.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
    <script src="assets/js/Simple-Slider.js"></script>

    <?php require_once("footer.php"); ?>