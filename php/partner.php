<?php
if(session_status() == PHP_SESSION_NONE) {
    session_start();
    }
    if(isset($_SESSION['type'])) {
     
    }
    else {
        header('Location: partner?type=0');
        $_SESSION['type'] = $_GET['type'];
    }
    

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Liste des partenaires</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abril+Fatface">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alfa+Slab+One">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora">
    <link rel="stylesheet" href="assets/fonts/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/Article-Clean.css">
    <link rel="stylesheet" href="assets/css/Article-List.css">
    <link rel="stylesheet" href="assets/css/Contact-Form-Clean.css">
    <link rel="stylesheet" href="assets/css/Features-Boxed.css">
    <link rel="stylesheet" href="assets/css/Footer-Dark.css">
    <link rel="stylesheet" href="assets/css/Highlight-Blue.css">
    <link rel="stylesheet" href="assets/css/Highlight-Clean.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css">
    <link rel="stylesheet" href="assets/css/Login-Form-Clean.css">
    <link rel="stylesheet" href="assets/css/Login-Form-Dark.css">
    <link rel="stylesheet" href="assets/css/Projects-Horizontal.css">
    <link rel="stylesheet" href="assets/css/Registration-Form-with-Photo.css">
    <link rel="stylesheet" href="assets/css/Simple-Slider.css">
    <link rel="stylesheet" href="assets/css/Team-Boxed.css">
    <link rel="stylesheet" href="assets/css/Navigation-with-Button.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body>
    <?php require_once('menu.php'); ?>
      
    <section class="article-list content">

    <div class="container">
        <div class="intro">
            <h2 class="text-center">La liste de nos partenaires  :</h2>
            <p class="text-center">Ci-dessous se trouve la liste des partenaires de l'écuries FOURILE </p>
        </div>
        <div class="row articles">
    <?php

// Affichage sur n colonnes
// Permet de réaliser l'affichage du résultat
// d'une requête dans un tableau sur n colonnes

require_once('BDD.php');

// Ouvre une connexion au serveur MySQL
$conn = mysqli_connect($db_server,$db_user_login , $db_user_pass,$db_name);

$req = "SELECT * FROM partner";
 
//--- Résultat ---//
$res = mysqli_query($conn,$req);
//met les données dans un tableau
while($data = mysqli_fetch_array($res))
{
$tablo[]=$data;
}
//détermine le nombre de colonnes
$nbcol=5;
$nb=count($tablo);
?>
<script type="text/javascript">
function image(img) {
    var src = img.src;
    window.open(src);
}
</script>
<?php
for($i=0;$i<$nb;$i++){
 
//les valeurs à afficher
$valeur1=$tablo[$i]['id'];
$valeur2=$tablo[$i]['name'];
$valeur3=$tablo[$i]['description'];
$valeur4=$tablo[$i]['link'];
$valeur5=$tablo[$i]['name_img'];
if($row = $res->fetch_assoc()) {
    
}
if($i%$nbcol==0)
echo '';
echo '<div class="col-sm-6 col-md-4 item"><img class="img-fluid" src="upload/' ,   $valeur5 ,  '"onclick="image(this)"></a>';
echo '<p><strong>Id : </strong>',$valeur1,'</p>' . "\r\n" . '<p><strong>Nom : </strong>',$valeur2, '</p>' . "\r\n" . '<p><strong> Description : </strong>' ,$valeur3, ' ans</p>' . "\r\n" . '<p><strong>Lien  : </strong><a onclick="window.open(this.src)" href=' ,$valeur4, '>' ,$valeur4, '</a></p>';
if($_SESSION['type'] == 1) {
    echo '<a class="btn btn-primary" href="partner_info.php?id=' . $valeur1 . '"> Voir fiche </a></div>';
}
else {
    echo ' </div>';
}



}
?>
        </div>
    </div>
</div>
</section>

    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
    <script src="assets/js/Simple-Slider.js"></script>
<?php require_once('footer.php'); ?>