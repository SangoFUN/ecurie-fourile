<?php
 if(session_status() == PHP_SESSION_NONE) {
  session_start();
  }
if($_SESSION['type'] == 1) {

}
else {
  header('Location: ./');
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Création de compte</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abril+Fatface">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alfa+Slab+One">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora">
    <link rel="stylesheet" href="assets/fonts/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/Article-Clean.css">
    <link rel="stylesheet" href="assets/css/Article-List.css">
    <link rel="stylesheet" href="assets/css/Contact-Form-Clean.css">
    <link rel="stylesheet" href="assets/css/Features-Boxed.css">
    <link rel="stylesheet" href="assets/css/Footer-Dark.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css">
    <link rel="stylesheet" href="assets/css/Projects-Horizontal.css">
    <link rel="stylesheet" href="assets/css/Registration-Form-with-Photo.css">
    <link rel="stylesheet" href="assets/css/Simple-Slider.css">
    <link rel="stylesheet" href="assets/css/Team-Boxed.css">
    <link rel="stylesheet" href="assets/css/Navigation-with-Button.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body>
  <?php require_once("menu.php"); ?>
      
    <section class="register-photo">
    <h1 class="title_user content"> Création d'un compte : </h1>
        <div class="form-container">
            <div class="image-holder"></div>
            <form method="post" action="validate_register.php">
                <h2 class="text-center"><strong>Crée</strong> un compte.</h2>
                <div class="form-group"><input class="form-control" type="text" name="last_name" placeholder="Nom"></div>
                <div class="form-group"><input class="form-control" type="text" name="first_name" placeholder="Prénom"></div>
                <div class="form-group"><input class="form-control" type="email" name="email" placeholder="Email"></div>
                <div class="form-group"><input class="form-control" type="email" name="confirm_email" placeholder="Répétez votre adresse Email"></div>
                <div class="form-group"><input class="form-control" type="password" name="password" placeholder="Mot de passe"></div>
                <div class="form-group"><input class="form-control" type="password" name="confirm_password" placeholder="Répétez votre mot de passe"></div>
  
                <div class="form-group">

                  <select class="form-control" name="type">
                    <option value="NULL"> Type de compte </option>
                    <option  value="1"> Administrateur </option>
                    <option  value="0"> Membre </option>
                  </select>
                </div>
                <div class="form-group"><button class="btn btn-primary btn-block" type="submit">Valider l'inscription</button></div>
            </form>
        </div>
    </section>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/bs-init.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
    <script src="assets/js/Simple-Slider.js"></script>

    <?php require_once("footer.php"); ?>