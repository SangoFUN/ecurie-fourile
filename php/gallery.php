<?php 
if(session_status() == PHP_SESSION_NONE) {
    session_start();
	if(isset($_SESSION['type'])) {
     
    }
    else {
        header('Location: gallery?type=0');
        $_SESSION['type'] = $_GET['type'];
    }
    }
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Galerie du club</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abril+Fatface">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alfa+Slab+One">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora">
    <link rel="stylesheet" href="assets/fonts/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/ionicons.min.css">
    <link rel="stylesheet" href="assets/fonts/fontawesome5-overrides.min.css">
    <link rel="stylesheet" href="assets/css/Article-Clean.css">
    <link rel="stylesheet" href="assets/css/Article-List.css">
    <link rel="stylesheet" href="assets/css/Contact-Form-Clean.css">
    <link rel="stylesheet" href="assets/css/Features-Boxed.css">
    <link rel="stylesheet" href="assets/css/Footer-Dark.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css">
    <link rel="stylesheet" href="assets/css/Projects-Clean.css">
    <link rel="stylesheet" href="assets/css/Projects-Horizontal.css">
    <link rel="stylesheet" href="assets/css/Simple-Slider.css">
    <link rel="stylesheet" href="assets/css/Team-Boxed.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link rel="stylesheet" href="assets/css/Navigation-with-Button.css">
    <link href="https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="assets/css/Bootstrap-4---Photo-Gallery.css">
</head>
<body>
<?php require_once('menu.php'); ?>
<div class="photo-gallery content" style="margin-top: 10%;  ">
<div class="container-fluid">
  <div class="px-lg-5">
    <div class="row">
        
      <!-- Gallery item -->
      <?php 
// Affichage sur n colonnes
// Permet de réaliser l'affichage du résultat
// d'une requête dans un tableau sur n colonnes

require_once('BDD.php');

// Ouvre une connexion au serveur MySQL
$conn = mysqli_connect($db_server,$db_user_login , $db_user_pass,$db_name);

$req = "SELECT * FROM gallery";
 
//--- Résultat ---//
$res = mysqli_query($conn,$req);
//met les données dans un tableau
while($data = mysqli_fetch_array($res))
{
$tablo[]=$data;
}
//détermine le nombre de colonnes
$nbcol=5;
$nb=count($tablo);
?>
<script type="text/javascript">
function image(img) {
    var src = img.src;
    window.open(src);
}
</script>
<?php
for($i=0;$i<$nb;$i++){
 
//les valeurs à afficher
$valeur1=$tablo[$i]['id'];
$valeur2=$tablo[$i]['name'];
$valeur3=$tablo[$i]['description_short'];
$valeur4=$tablo[$i]['description_long'];
$valeur5=$tablo[$i]['name_img'];
if($row = $res->fetch_assoc()) {
    
}
if($i%$nbcol==0)
    echo ' ';
    echo '<div class="col-xl-3 col-lg-4 col-md-6 mb-4">';
        echo '<div class="bg-white rounded shadow-sm "><img src="upload/' ,$valeur5, '" alt="" class=" ik img-fluid card-img-top">';
         echo '<div class="p-4">';
         echo '<h5> <a href="gallery_info.php?id=', $valeur1, '" class="text-dark">' , $valeur2, '</a></h5>';
          echo '<p class="small text-muted mb-0">' , $valeur3, '</p>';
        echo '</div>';
        echo '</div>';
    echo '</div>';  


}
?> 
</div>
<?php
if($_SESSION['type'] == 1) {
    echo '<div class="py-5 text-right"><a href="register_gallery.php  " class="btn btn-dark px-5 py-3 text-uppercase">Ajouter une nouvelle photo </a></div>'; 
  
}
else {
    echo'';
}
?>
</div> 
</div> 
</div> 
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/bs-init.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
    <script src="assets/js/Simple-Slider.js"></script>
<?php require_once('footer.php'); ?>    