<?php
if(session_status() == PHP_SESSION_NONE) {
    session_start();
    }
if($_SESSION['type'] == 1) {

}
else {
  header('Location: horse');
}
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Profil du Cheval</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abril+Fatface">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alfa+Slab+One">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora">
    <link rel="stylesheet" href="assets/fonts/ionicons.min.css">
    <link rel="stylesheet" href="assets/css/Article-Clean.css">
    <link rel="stylesheet" href="assets/css/Article-List.css">
    <link rel="stylesheet" href="assets/css/Contact-Form-Clean.css">
    <link rel="stylesheet" href="assets/css/Features-Boxed.css">
    <link rel="stylesheet" href="assets/css/Footer-Dark.css">
    <link rel="stylesheet" href="assets/css/Highlight-Blue.css">
    <link rel="stylesheet" href="assets/css/Highlight-Clean.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css">
    <link rel="stylesheet" href="assets/css/Login-Form-Clean.css">
    <link rel="stylesheet" href="assets/css/Login-Form-Dark.css">
    <link rel="stylesheet" href="assets/css/Projects-Horizontal.css">
    <link rel="stylesheet" href="assets/css/Registration-Form-with-Photo.css">
    <link rel="stylesheet" href="assets/css/Simple-Slider.css">
    <link rel="stylesheet" href="assets/css/Team-Boxed.css">
    <link rel="stylesheet" href="assets/css/Navigation-with-Button.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body>
    <?php require_once('menu.php'); ?>
<section class="article-list content">
<div class="container">
    <div class="intro">
<?php 
        require_once('BDD.php');
        
        $conn = mysqli_connect($db_server,$db_user_login , $db_user_pass,$db_name);
        
        
        if (isset($_GET['id']))
    {
        $id = $_GET['id'];
        $req = "SELECT * FROM horse_info WHERE id='$id'";
        
        $res = mysqli_query($conn,$req);
 
        
        if ($res->num_rows > 0) {
            
            if($row = $res->fetch_assoc()) {
                $image = $row['name_img'];
                $image_src = "upload/".$image;
                echo '<h2 class="text-center">Profil du cheval ' . $row["name"] . '</h2>';
                echo'<p class="text-center"><strong>Id : </strong>'.$row["id"].'</p>' . "\r\n" . '';
                echo '<p class="text-center"><strong>Nom : </strong>'.$row["name"].'</p>' . "\r\n" . '';
                echo '<p class="text-center"><strong>Age : </strong>'.$row["age"].'</p>' . "\r\n" . '';
                echo '<p class="text-center"><strong>Nom du père : </strong>'.$row["name_father"].'</p>' . "\r\n" . '';
                echo '<p class="text-center"><strong>Nom de la mère : </strong>'.$row["name_mother"].'</p>' . "\r\n" . '';
                echo '<p class="text-center"><strong>Nom du père de la mère : </strong>'.$row["name_father_of_mother"].'</p>' . "\r\n" . '';
                echo '<p class="text-center"><strong>Description : </strong>'.$row["description"].'</p>' . "\r\n" . ''; 
                echo '</div>';
                echo '<div class="container">';
                echo '<div class="row">';
                echo '<div class="col-md-12"><img src="upload/' ,   $row['name_img'] ,  '"  style="width: 100%;height: 100%;padding-top:2%;padding-bottom:2%;"></div>';
                echo '</div>';
                echo '<div class="row">';
                echo '<div class="col-md-12">';   
                echo '<section class="projects-clean">';
                    echo '<div class="container">';
                        echo '<div class="intro"></div>';
                        echo '<div class="row projects">' ;             
                             if(isset($_SESSION['type'])) {
                                if($_SESSION['type'] == 1) {
                                    echo '<a class="btn btn-primary center" style="margin-bottom:5%;margin-left: 30%;" href="change_horse_info.php?id=' .$row["id"]. '"> Modifier le cheval</a>';
                                    echo '<a class="btn btn-primary center" style="margin-bottom:5%;margin-left: 10%;" href="delete_horse.php?id=' .$row["id"]. '"> Supprimer le cheval </a>';
                                }
                            }
                            else {
                                
                                }
                            echo '</div>';
                        echo '</div>';
                    echo '</div>';
                echo '</section>';
                echo '</section>';
                echo '</div>';
            }
        }
        else {
           echo "0 results";
        }
        

        
    }
?>
        </div>
    </div>
</div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
    <script src="assets/js/Simple-Slider.js"></script>
<?php require_once('footer.php'); ?>