<?php
 if(session_status() == PHP_SESSION_NONE) {
    session_start();
    }

?>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        <link rel="stylesheet" href="assets/css/calendar.css">
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abril+Fatface">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alfa+Slab+One">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora">
        <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
        <link rel="stylesheet" href="assets/css/Article-Clean.css">
        <link rel="stylesheet" href="assets/css/Article-List.css">
        <link rel="stylesheet" href="assets/css/Contact-Form-Clean.css">
        <link rel="stylesheet" href="assets/css/Features-Boxed.css">
        <link rel="stylesheet" href="assets/css/Footer-Dark.css">
        <link rel="stylesheet" href="assets/css/Highlight-Blue.css">
        <link rel="stylesheet" href="assets/css/Highlight-Clean.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css">
        <link rel="stylesheet" href="assets/css/Login-Form-Clean.css">
        <link rel="stylesheet" href="assets/css/Login-Form-Dark.css">
        <link rel="stylesheet" href="assets/css/Projects-Horizontal.css">
        <link rel="stylesheet" href="assets/css/Registration-Form-with-Photo.css">
        <link rel="stylesheet" href="assets/css/Simple-Slider.css">
        <link rel="stylesheet" href="assets/css/Team-Boxed.css">
        <link rel="stylesheet" href="assets/css/Navigation-with-Button.css">
        <link rel="stylesheet" href="assets/css/style.css">
        <title><?= isset($title) ? h($title)  : "Mon Planning "; ?></title>
    </head>
    <body>
    <nav class="navbar navbar-light navbar-expand-md navigation-clean-button fixed-top menuu" style="background: #282d32;">
        <div class="container fontmenu"><a class="navbar-brand" href="./" style="color: rgb(255,255,255);"><img src="assets/img/fav.png" style="max-width:48px;max-height:48px;">   Ecuries FOURILE</a><button data-toggle="collapse" class="navbar-toggler" data-target="#navcol-1" style="border-color: rgb(136, 136, 136);border-top-color: rgb(136,;border-right-color: 136,;border-bottom-color: 136);border-left-color: 136,;"><span class="sr-only">Toggle navigation</span><span class="navbar-toggler-icon"></span></button>
            <div class="collapse navbar-collapse" id="navcol-1">
                <ul class="navbar-nav mr-auto">
                <?php 
                if(isset($_SESSION['logged']) && $_SESSION['logged'] == true && $_SESSION['type'] == 1) {
                  echo"<li class=\"nav-item\">" . 
                    "<a class=\"nav-link\" style=\"color: rgb(170,170,170);\" href=\"admin\">Administration</a>" . 
                  "</li>";
                }
                else {

                }
                ?>
                <li class="nav-item">
                  <a class="nav-link" style="color: rgb(170,170,170);" href="./">Accueil
                      </a>
                </li>
                <li class="nav-item">
                  <div class="dropdown">
                  <a class="dropdown-toggle nav-link" aria-expanded="false" data-toggle="dropdown" type="button" style="background: #282d32;border-top-color: rgb(52,58,64);border-right-color: rgb(52,58,64);border-bottom-color: rgb(52,58,64);border-left-color: rgb(52,58,64);color: rgb(170,170,170);">Club</a>
                    <div class="dropdown-menu">
                        <a class="dropdown-item" href="activity">Activités proposées</a>
                        <a class="dropdown-item" href="emploi_du_temps">Emploi du temps</a>
                        <a class="dropdown-item" href="gallery">Galerie Photos</a>
                        <a class="dropdown-item" href="future_events">Événements à venir ...</a>
                    </div>
                </li>
                <li class="nav-item">
                  <a class="nav-link" style="color: rgb(170,170,170);" href="tarif">Tarifs</a>
                </li>
                <?php
                if(isset($_SESSION['logged']) && $_SESSION['logged'] == true) {
                  echo"<li class=\"nav-item\">" . 
                    "<a class=\"nav-link\" style=\"color: rgb(170,170,170);\" href=\"planning_cours\">Planning</a>" . 
                  "</li>";
                } 
                ?>
                <li class="nav-item">
                  <a class="nav-link" style="color: rgb(170,170,170);" href="send_devis">Demande de devis</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" style="color: rgb(170,170,170);" href="horse">Chevaux à vendre</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" style="color: rgb(170,170,170);" href="partner">Partenaires</a>
                </li>
                <li class="nav-item">
                  <a class="nav-link" style="color: rgb(170,170,170);" href="contact">Contact</a>
                </li>
                
                <?php 
                  if(isset($_SESSION['logged']) && $_SESSION['logged'] == true) {
                    echo"<li class=\"nav-item\">" . 
                    "<a class=\"nav-link\" style=\"color: rgb(170,170,170);\" href=\"disconnect\">Déconnexion</a>" . 
                  "</li>";

                  } else {
                    
                  }
                ?>
                                <?php 
                  if(isset($_SESSION['logged']) && $_SESSION['logged'] == true) {
                    
                  echo'</ul> 
                   <span class="navbar-text actions"> <p class="btn btn-light action-button" role="button" style="color:#FFF;" > ' . " " .  $_SESSION['last_name'] . " " . $_SESSION['first_name'] . 
                    '</p></span>';

                  } else {
                    
                  }
                ?>
                <?php 
                if(isset($_SESSION['logged']) && $_SESSION['logged'] == true ) {
                  
                }
                else {
                  echo'</ul> 
                   <span class="navbar-text actions">  <a class="btn btn-light action-button" role="button" style="color:rgb(170,170,170);" href="login">
               Se connecter</a>';
                  
                }
                ?>
              </ul>
            </div>
          </div>
      </nav>
            </div>
        </div>
    </nav>
    <script src="assets/js/checkHeader.js"></script>