<?php
 if(session_status() == PHP_SESSION_NONE) {
    session_start();
    }
    if($_SESSION['type'] == 0) {
        header('Location: planning_cours');
        exit();
    }

require_once("src/Bootstrap.php");
require_once("src/App/Validator.php");
require_once("src/Calendar/EventValidator.php");
require_once("src/Calendar/Events.php");
require_once("src/Calendar/Event.php");

render('header', ['title' => 'Ajouter un événement']);

$data = [
    'date' => $_GET['date'] ?? date('Y-m-d'),
    'start' => date('H:i'),
    'end' => date('H:i')
];

$validator = new \App\Validator($data);
if(!$validator->validate('date', 'date')) {
    $data['date'] = date('Y-m-d');
}
$errors = [];
if($_SERVER['REQUEST_METHOD'] === 'POST') {
    $data = $_POST;
    $errors = [];
    $validator = new Calendar\EventValidator();
    $errors = $validator->validates($_POST);
    if(empty($errors)) {
        
        $events = new \Calendar\Events(get_pdo());
        $event = $events->hydrate(new \Calendar\Event(), $data);
        $events->create($event);
        sleep(3);
        echo "<script language=\"javascript\">"
        . "alert('Vous avez bien ajouter l\'évènement, vous allez être rediriger vers la page de planning :')"  .  "</script>"
          . "<script language=\"javascript\">" .  "window.location.replace('planning_cours.php');" .  "</script>";
    }

}
?>

<div class="container content">
    <h1> Ajouter un évènement</h1>
    <form action="" method="post" class="form content">
    <?php render('/Calendar/form', ['data' => $data, 'errors' => $errors]); ?>
        
        <div class="form-group">
            <button class="btn btn-primary"> Ajouter l'évènement</button>
    </form>
        </div>
</div>

<?php render('/footer'); ?>