<?php
if (session_status() == PHP_SESSION_NONE) {
    session_start();
    }

?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Accueil</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abril+Fatface">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alfa+Slab+One">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora">
    <link rel="stylesheet" href="assets/fonts/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/ionicons.min.css">
    <link rel="stylesheet" href="assets/fonts/fontawesome5-overrides.min.css">
    <link rel="stylesheet" href="assets/css/Article-Clean.css">
    <link rel="stylesheet" href="assets/css/Article-List.css">
    <link rel="stylesheet" href="assets/css/Contact-Form-Clean.css">
    <link rel="stylesheet" href="assets/css/Features-Boxed.css">
    <link rel="stylesheet" href="assets/css/Footer-Dark.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css">
    <link rel="stylesheet" href="assets/css/Projects-Clean.css">
    <link rel="stylesheet" href="assets/css/Projects-Horizontal.css">
    <link rel="stylesheet" href="assets/css/Simple-Slider.css">
    <link rel="stylesheet" href="assets/css/Team-Boxed.css">
    <link rel="stylesheet" href="assets/css/Navigation-with-Button.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link href="https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap" rel="stylesheet">
</head>

<body>
    <?php require_once("menu.php"); ?>
    <script type="text/javascript">
        function image(img) {
             var src = img.src;
             window.open(src);
}
</script>
    <!-- Site réalisé par YUKSEL Sergen, le 18 juin 2021. -->
    <div class="simple-slider" >
        <div class="swiper-container  pad">
            <div class="swiper-wrapper">
                <div class="swiper-slide" style="background: url(&quot;assets/img/D2Lphoto.jpg&quot;) center center / cover no-repeat;opacity: 1;filter: contrast(111%) grayscale(0%);"></div>
                <div class="swiper-slide" style="background: url(&quot;upload/IMG_2118.jpg&quot;) center center / cover no-repeat;"></div>
                <div class="swiper-slide" style="background: url(&quot;upload/IMG_2145.jpg&quot;) center center / cover no-repeat;"></div>
                <div class="swiper-slide" style="background: url(&quot;upload/IMG_2058.jpg&quot;) center center / cover no-repeat;"></div>
                <div class="swiper-slide" style="background: url(&quot;upload/modif1.jpg&quot;) center center / cover no-repeat;"></div>
                <div class="swiper-slide" style="background: url(&quot;upload/modif2.jpg&quot;) center center / cover no-repeat;"></div>
            </div>
            <div class="swiper-pagination "></div>
            <div class="swiper-button-prev"></div>
            <div class="swiper-button-next" ></div>
        </div>
    </div>
    <section class="features-boxed">
        <div class="container">
            <div class="intro">
                <h2 class="text-center">Sommaire </h2>
            </div>
            <div class="row justify-content-center features">
                <div class="col-sm-6 col-md-5 col-lg-4 item">
                <div class="box"><svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" class="bi bi-house-fill icon" style="color : #46a321;">
                            <path fill-rule="evenodd" d="M8 3.293l6 6V13.5a1.5 1.5 0 0 1-1.5 1.5h-9A1.5 1.5 0 0 1 2 13.5V9.293l6-6zm5-.793V6l-2-2V2.5a.5.5 0 0 1 .5-.5h1a.5.5 0 0 1 .5.5z"></path>
                            <path fill-rule="evenodd" d="M7.293 1.5a1 1 0 0 1 1.414 0l6.647 6.646a.5.5 0 0 1-.708.708L8 2.207 1.354 8.854a.5.5 0 1 1-.708-.708L7.293 1.5z"></path>
                        </svg>
                        <h3 class="name">Notre Structure</h3>
                        <p class="description" >Écurie de propriétaires  actuellement située à Balan (08200), nous vous proposons des pensions et des demi-pensions.</p><a class="learn-more" href="#structure" style="color : #343a40;">En savoir plus »</a>
                </div>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-4 item">
                        <div class="box"><svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" class="bi bi-book icon" style="color : #d90d0d;">
                            <path fill-rule="evenodd" d="M1 2.828v9.923c.918-.35 2.107-.692 3.287-.81 1.094-.111 2.278-.039 3.213.492V2.687c-.654-.689-1.782-.886-3.112-.752-1.234.124-2.503.523-3.388.893zm7.5-.141v9.746c.935-.53 2.12-.603 3.213-.493 1.18.12 2.37.461 3.287.811V2.828c-.885-.37-2.154-.769-3.388-.893-1.33-.134-2.458.063-3.112.752zM8 1.783C7.015.936 5.587.81 4.287.94c-1.514.153-3.042.672-3.994 1.105A.5.5 0 0 0 0 2.5v11a.5.5 0 0 0 .707.455c.882-.4 2.303-.881 3.68-1.02 1.409-.142 2.59.087 3.223.877a.5.5 0 0 0 .78 0c.633-.79 1.814-1.019 3.222-.877 1.378.139 2.8.62 3.681 1.02A.5.5 0 0 0 16 13.5v-11a.5.5 0 0 0-.293-.455c-.952-.433-2.48-.952-3.994-1.105C10.413.809 8.985.936 8 1.783z"></path>
                        </svg>
                        <h3 class="name">Notre Histoire</h3>
                        <p class="description">Mme Betty.F et Mme Chloé.F racontent leur histoire à propos de leurs études et comment elles ont pu créer l'écurie FOURILE.</p>    <a class="learn-more" href="#histoire" style="color : #343a40;">En savoir plus »</a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-4 item">
                    <div class="box"><i class="fa fa-group icon"style="color : #000080;"></i>
                        <h3 class="name">Notre Equipe </h3>
                        <p class="description"><br><br></p><br><br><a class="learn-more" href="#equipe" style="color : #343a40;">En savoir plus »</a>
                    </div>
                </div>
                <div class="col-sm-6 col-md-5 col-lg-4 item">
                    <div class="box"><svg xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" class="bi bi-wallet-fill icon" style="color : #f0c60a;">
                            <path d="M1.5 2A1.5 1.5 0 0 0 0 3.5v2h6a.5.5 0 0 1 .5.5c0 .253.08.644.306.958.207.288.557.542 1.194.542.637 0 .987-.254 1.194-.542.226-.314.306-.705.306-.958a.5.5 0 0 1 .5-.5h6v-2A1.5 1.5 0 0 0 14.5 2h-13z"></path>
                            <path d="M16 6.5h-5.551a2.678 2.678 0 0 1-.443 1.042C9.613 8.088 8.963 8.5 8 8.5c-.963 0-1.613-.412-2.006-.958A2.679 2.679 0 0 1 5.551 6.5H0v6A1.5 1.5 0 0 0 1.5 14h13a1.5 1.5 0 0 0 1.5-1.5v-6z"></path>
                        </svg>
                        <h3 class="name">Les pensions et demi-pensions </h3>
                        <p class="description">Les différents produits et services proposés par l'écurie</p><br><a class="learn-more" href="#pension" style="color : #343a40;">En savoir plus »</a>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <script>

$(window).on('scroll', function() {
   window.requestAnimationFrame(scrollHandler);
});

</script>
    <div id="structure" data-bss-parallax-bg="true" style="background: url(&quot;assets/img/IMG_2176.JPG&quot;)center / cover;" class="image_fond_responsive">
    <section class="highlight-clean backgroundf" style="background: rgba(255,255,255,0);">
            <section class="article-list" style="filter: blur(0px) contrast(102%) grayscale(0%) hue-rotate(0deg) invert(0%) sepia(0%);background: transparent;">
                <div class="container" style="margin-top: -302px;">
                    <div class="row articles">
                        <div class="col-sm-6 col-md-4 item"><img class="img-fluid" src="assets/img/IMG_20210611_020922_01.jpg" onclick="image(this)">
                            <h3 class="name structure">Une carrière 50 x 30</h3>
                            <p class="description"></p><a class="action" href="#"></a>
                        </div>
                        <div class="col-sm-6 col-md-4 item"><img class="img-fluid" src="assets/img/IMG_20210611_021242_01.jpg" onclick="image(this)">
                            <h3 class="name structure">Un manège 50 x 23</h3>
                            <p class="description"></p><a class="action" href="#"></a>
                        </div>
                        <div class="col-sm-6 col-md-4 item"><img class="img-fluid" src="assets/img/IMG_20210611_023526_01.jpg" onclick="image(this)">
                            <h3 class="name structure">Une douche avec eau chaude et solarium</h3>
                            <p class="description"></p><a class="action" href="#"></a>
                        </div>
                        <div class="col-sm-6 col-md-4 item"><img class="img-fluid" src="assets/img/IMG_20210611_023532_01.jpg" onclick="image(this)">
                            <h3 class="name structure">Un club house</h3>
                            <p class="description"></p><a class="action" href="#"></a>
                        </div>
                        <div class="col-sm-6 col-md-4 item"><img class="img-fluid" src="assets/img/IMG_20210611_023536_01.jpg" onclick="image(this)">
                            <h3 class="name structure">Une sellerie</h3>
                            <p class="description"></p><a class="action" href="#"></a>
                        </div>
                        <div class="col-sm-6 col-md-4 item"><img class="img-fluid" src="assets/img/IMG_20210611_023539_01.jpg" onclick="image(this)">
                            <h3 class="name structure">Des paddocks</h3>
                            <p class="description"></p><a class="action" href="#"></a>
                        </div>
                    </div>
                </div>
            </section>
    </section>
</div>
<section id="histoire" class="projects-clean content menuu" style="background-color:#eef4f7;">
        <div class="container ">
            <div class="intro">
                <h2 class="text-center">Notre Histoire</h2>
                <p class="text-center">La genèse de l'Ecurie Fourile</p>
            </div>
            <div class="row projects">
                <div class="col-sm-6 col-lg-4 item fd">
                <h3 class="name"style="margin-bottom: 40px;">Betty FOURILE</h3><img class="phot img-fluid" src="assets/img/IMG_20210614_141527_01.jpg" onclick="window.open(this.src)">
                    <div class="fancy-border">
                        <div class="cadre" style ="border: 5px solid #b78846 ;border-style: double;border-width: 10px;margin-left: 2%;border-radius: 6px 6px 6px 6px;"> <p class="description max-space" style="font-family:Loa;"><br>J’ai suivi une formation en BEPA soigneur et aide animateur en 2008 pour une durée de 2 ans en apprentissage chez Isabelle Chevrier au poney club la Marfée à Frénois ensuite, j’ai complété ma formation par un BAC PRO CGEA option élevage en 2010 pour une durée de 2 ans.<br>J’ai effectué ma première année dans l’élevage de Sorel à Damouzy chez Dominique Fostier, j’étais livrée à moi-même et ma soif, d’apprendre d’avantage m’a poussé à faire ma deuxième année dans une autre structure équestre. Je suis donc allée dans une écurie de propriétaires à Noyers-Pont-Maugis chez Marie-Ange Malvy où j’ai beaucoup appris à gérer les propriétaires et leurs montures.<br>Je ne comptais pas m’arrêter là, en 2012, je me suis lancé dans un BPJEPS mention équitation pour pouvoir enseigner mon savoir-faire et j’ai effectué mon stage au centre équestre de Douzy chez Christian Caron.<br>&nbsp;<br>Dès l’obtention de mon diplôme, &nbsp;j’ai passé un entretien à l’Étrier Ardennais qui recherchait un moniteur. J’ai été embauchée sur-le-champ.<br>J’y ai passé un peu plus de trois années à enseigner à des personnes de tous âges, de 3 à 70 ans environ. J’étais quelqu’un de très appréciée de par mon professionnalisme et pour mon ambition de voir toujours plus loin.<br>Nous avons organisé beaucoup de concours, de stages, de randonnées. Nous avons aussi créé des couples cavalier/cheval pour aller en concours à l’extérieur et pouvoir se qualifier pour les championnats de France en 2016 où j’ai eu l’honneur de monter sur la 3e marche du podium avec une de mes cavalières en épreuve de dressage. Après toutes ces bonnes années, j’ai décidé de prendre mon envol. J’ai créé mon entreprise de monitrice indépendante et une quinzaine de clients propriétaires m’ont suivi pour que je puisse continuer à leur donner cours. J’avais également un mi-temps dans l’entreprise des Cavaliers du Parc chez Emmanuelle Dulin en tant que cavalière pour monter les chevaux d’élevage et les valoriser en concours.<br>&nbsp;<br>Cette année-là, en 2017, j’ai été enceinte de mon petit Tilou, j’ai donc dû mettre ma carrière de cavalière de côté. Malgré tout, j’ai continué à enseigner jusqu’à mon congé maternité.<br>Après la naissance de mon petit, j’ai vite remis le pied à l’étrier. J’ai démissionné des cavaliers du parc, car j’avais comme ambition de créer ma propre écurie. Le projet a vu le jour en septembre 2018 avec le soutien de ma sœur et de mes parents.<br>Tous les clients propriétaires m’ont suivie et sont encore là aujourd’hui. Nous avons déjà vécu de belles choses, de belles randonnées, différents stages, de beaux concours comme celui du salon du cheval où ma cavalière a remporté l’épreuve Club 2 en étant à la tête du classement sur les 2 jours du concours.<br>&nbsp;<br>Nous comptons bien vivre encore bien d’autres moments forts et encore plus enrichissants de par la taille de notre futur projet&nbsp;!!!<br><br></p></div></div>
                </div>
                <div class="col-sm-6 col-lg-4 item fd">
                    <h3 class="name"style="margin-bottom: 40px;">Chloé FOURILE</h3><img class="phots img-fluid" src="assets/img/image2.jpeg" onclick="window.open(this.src)">
                    <div class="cadre" style ="border: 5px solid #b78846 ;border-style: double;border-width: 10px;margin-left: 2%;border-radius: 6px 6px 6px 6px;"><p class="description max-space" style="font-family:Loa;">Je m'appelle Chloé FOURILE, je suis titulaire du  BPJEPS depuis juin 2020.<br><br>J'ai obtenu un BAC pro Conduite et Gestion d'un Elevage Canin et Félin en 2009. N'ayant pas de débouché dans la filière, j'ai repris les études avec l'obtention d'un CAP coiffure.&nbsp;<br>Suite à une maladie professionnelle, j'ai dû arrêter la coiffure en 2017. Après une période de convalescence, étant cavalière depuis l'âge de mes 3 ans et prenant cours régulièrement avec Alexandra Francart, cavalière professionnelle, j'ai eu l'opportunité d'être sa groom durant une saison en 2018.<br>Nous avons participé à de très beaux concours avec son cheval Volnay du Boisdeville comme la Baule, Saint-Galles où elle a remporté la coupe des nations pour finir aux jeux équestres mondiaux à Tryon en septembre 2018, juste avant l'ouverture de nos écuries à Balan.<br><br>Alexandra Francart, quittant les Ardennes, nous a proposées à ma sœur Betty FOURILE qui est monitrice d'équitation depuis 10 ans et moi-même de reprendre ses écuries.<br><br>Ce projet m'a permis de débuter ma reconversion professionnelle et pour ma sœur et moi de créer notre entreprise familiale.</p>
                </div>
            </div>
        </div>
    </section>
    <section id="equipe" class="team-boxed content" style="background-color:#eef4f7;">    
        <div class="container wid" style="min-width:100%;">
            <div class="intro">
                <h2 class="text-center">Equipe </h2>
                <p class="text-center">Ci-dessous se trouve notre équipe : </p>
            </div>
            <div class="row people">
                <div class="col-md-6 col-lg-4 item">
                    <div class="box mobilebox">
                        <h3 class="name namemargin">Mme Betty FOURILE</h3>
                        <p class="title titlemargin">GÉrante</p>
                        <p class="description descriptionmargin">Titulaire du BPJEPS (Le brevet professionnel de la jeunesse, de l’éducation populaire et du sport) équitation. </p>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 item">
                    <div class="box mobilebox">
                        <div class="social"><img style="min-width:90%;margin-left:3%;" src="assets/img/IMG_20210610_151332_01.jpg" onclick="window.open(this.src)"><a href="#"></a><a href="#"></a><a href="#"></a></div>
                    </div>
                </div>
                <div class="col-md-6 col-lg-4 item">
                    <div class="box mobilebox">
                        <h3 class="name namemargin">Mme Chloé FOURILE</h3>
                        <p class="title titlemargin">GÉrante</p>
                        <p class="description descriptionmargin">Titulaire du BPJEPS (Le brevet professionnel de la jeunesse, de l’éducation populaire et du sport) équitation. </p>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div id="pension" data-bss-parallax-bg="true" style="background: url(&quot;assets/img/IMG_2136.jpg&quot;) center / cover;" class="image_fond_responsive2">
        <section class="highlight-clean" style="padding: 323px 0px;background: rgba(255,255,255,0);">
            <div class="container" style="border: 5px solid #FFF;background: #eef4f7;">
                <div class="intro">
                    <h2 class="text-center" style="margin-bottom: 26px;font-size: 64px;color: #282d32;filter: brightness(0%) contrast(108%);">Les pensions et demi-pensions</h2>
                    <p  style="font-size: 18px;color: var(--gray-dark);text-shadow: 1px 0px #282d32;filter: brightness(47%);margin-top: 86px;">Nous proposons des pensions boxes à partir de 300 €.
                    Les chevaux sont nourris 2 fois par jour en foin et 3 fois par jour en granulés, ils sont paillés plusieurs fois dans la semaine.  <br><br>
                    Les propriétaires disposent : d'un manège, d'une carrière, d'une sellerie avec casier compris dans la pension, d'un club house, d'une douche avec eau chaude  , d'un solarium et 4 paddocks. <br><br>
                    Vous pouvez également ajouter des sorties paddock, longe. Nous pouvons travailler votre cheval occasionnellement, 2, 4 ou 5 fois par semaine. <br><br>
                    Chaque semaine, nous proposons des cours collectifs ou particuliers pour tous niveaux. Il est possible également de passer les galops. <br><br>
                    Vous rêvez d’avoir votre propre cheval mais cela représente un coût trop important pour vous? <br><br>
                    Nous avons une alternative pour vous ! Nous vous proposons plusieurs demi-pensions sur différents chevaux. Possibilité de prendre des cours de dressage, d’obstacles... Sorties en balade et possibilité de sortir en concours. Tout est possible, la seule condition est d'être autonome à cheval/poney. <br><br>
                    N’hésitez pas à prendre contact avec nous, nous vous ferons un plaisir de vous présenter nos chevaux disponibles. Vous pouvez les retrouver dans l'onglet "chevaux à vendre". Option d’achat possible en fin de contrat si bonne entente avec le cheval !
 </p>
                </div>
                <div class="buttons"></div>
            </div>
        </section> 
    </div>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/bs-init.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
    <script src="assets/js/Simple-Slider.js"></script>

    <?php require_once("footer.php") ?>
    
    