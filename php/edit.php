<?php
 if(session_status() == PHP_SESSION_NONE) {
    session_start();
    }
if($_SESSION['type'] == 0) {
    header('Location: planning_cours');
    exit();
}
    require_once("src/Bootstrap.php");
    require_once("src/App/Validator.php");
    require_once("src/Calendar/EventValidator.php");
    require_once("src/Calendar/Events.php");
    require_once("src/Calendar/Event.php");
  
    $pdo = get_pdo();
    $events = new \Calendar\Events(get_pdo());
    $errors = [];
    try {
        $event = $events->find($_GET['id'] ?? null);
    } catch(\Exception $e) {
        e404();
    }
      catch(\Error $e) {
        e404();
    }
    $data = [
        'name' => $event->getName(),
        'date' => $event->getStart()->format('Y-m-d'),
        'start' => $event->getStart()->format('H:i'),
        'end' => $event->getEnd()->format('H:i'),
        'description' => $event->getDescription()
    ];

    if($_SERVER['REQUEST_METHOD'] === 'POST') {
        $data = $_POST;
        $validator = new Calendar\EventValidator();
        $errors = $validator->validates($data);
        if(empty($errors)) {
            $events->hydrate($event, $data);
            $events->update($event);
            header('Location: planning_cours?success=1');
            exit();
        }
    
    }
    render("header", ['title' => $event->getName()]);
    ?>

    <div class="container content ">
    <h1>Editer l'évènement <small><?= h($event->getName()); ?></small></h1>

    <form action="" method="post" class="form">
    <?php render('/Calendar/form', ['data' => $data, 'errors' => $errors]); ?>
        
        <div class="form-group">
            <button class="btn btn-primary"> Modifier l'évènement</button>
    </form>
    <a class="btn btn-primary" href="event?id=<?= $event->getId(); ?>"> Visualiser l'évènement</a>
    <a class="btn btn-primary" href="delete?id=<?= $event->getId(); ?>"> Supprimer l'évènement</a>
        </div>
    </div>

