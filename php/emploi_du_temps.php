<?php 
if(session_status() == PHP_SESSION_NONE) {
    session_start();
	if(isset($_SESSION['type'])) {
     
    }
    else {
        header('Location: emploi_du_temps?type=0');
        $_SESSION['type'] = $_GET['type'];
    }
    }
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
	<meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Emploi du temps</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abril+Fatface">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alfa+Slab+One">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora">
    <link rel="stylesheet" href="assets/fonts/fontawesome-all.min.css">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="assets/fonts/ionicons.min.css">
    <link rel="stylesheet" href="assets/fonts/fontawesome5-overrides.min.css">
    <link rel="stylesheet" href="assets/css/Article-Clean.css">
    <link rel="stylesheet" href="assets/css/Article-List.css">
    <link rel="stylesheet" href="assets/css/Contact-Form-Clean.css">
    <link rel="stylesheet" href="assets/css/Features-Boxed.css">
    <link rel="stylesheet" href="assets/css/Footer-Dark.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css">
    <link rel="stylesheet" href="assets/css/Projects-Clean.css">
    <link rel="stylesheet" href="assets/css/Projects-Horizontal.css">
    <link rel="stylesheet" href="assets/css/Simple-Slider.css">
    <link rel="stylesheet" href="assets/css/Team-Boxed.css">
    <link rel="stylesheet" href="assets/css/style.css">
    <link href="https://fonts.googleapis.com/css2?family=Abril+Fatface&display=swap" rel="stylesheet">
	<link rel="stylesheet" href="assets/css/Navigation-with-Button.css">
	<link rel="stylesheet" href="assets/css/calendar.css">
	<link rel="stylesheet" href="assets/css/edt.css">
</head>
<body>
<?php require_once('menu.php'); ?>
<div class="table-responsive content">
	<table id="planning" class=" table content" style="margin-top: 100px;">
	<thead>
		<tr>
			<th scope="col"></th>
			<th scope="col">LUNDI</th>
			<th scope="col">MARDI</th>
			<th scope="col">MERCREEDI</th>
			<th scope="col">JEUDI</th>
			<th scope="col">VENDREDI</th>
			<th scope="col">SAMEDI</th>
			<th scope="col">DIMANCHE</th>
		</tr>
			<?php
				$hour_start = 8;
				$hour_end = 21;
				for ($i = $hour_start; $i <= $hour_end; $i++) {
					echo "<tr>";
					echo '<td scope="row">' . $i . 'h</td>';
					for ($j=0; $j < 7; $j++) {
						echo "<td></td>";
					}
					echo "</tr>";
				}
			?>
		</tr>
	</table>
</div>

	<br><br>
	<?php 
	if($_SESSION['type'] == 1) { ?>
		<?= '<form method="POST" action="">'; ?>
		<?= ' <div style="margin-bottom:10%; class="illustration"><svg style="margin-left: 100px; xmlns="http://www.w3.org/2000/svg" width="1em" height="1em" viewBox="0 0 16 16" fill="currentColor" class="bi bi-trash" style="color: rgb(52,58,64);border-color: rgb(52,58,64);">
                    <path d="M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z"></path>
                    <path fill-rule="evenodd" d="M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z"></path>
                </svg>'; ?>
		<?= '<input style="margin-right:5%;margin-left:1%;" type="checkbox" name="delete" id="delete">' ; ?>
		<?= '<select id="day" name="day">'; ?>
		<?= '	<option>--Jour de la semaine--</option>' ; ?>
		<?= '	<option value="0">LUNDI</option>' ; ?>
		<?= '	<option value="1">MARDI</option>' ; ?>
		<?= '	<option value="2">MERCREEDI</option>' ; ?>
		<?= '	<option value="3">JEUDI</option>' ; ?>
		<?= '	<option value="4">VENDREDI</option>' ; ?>
		<?= '	<option value="5">SAMEDI</option>' ; ?>
		<?= '	<option value="6">DIMANCHE</option>' ; ?>
		<?= '</select>' ; ?>
		<?= '<input id="hours" type="number" name="hours" placeholder="Heure">' ; ?>
		<?= '<input id="text" type="text" name="text" placeholder="Evènement ">' ; ?>
		<?= '<select id="color" name="color">'; ?>
		<?= '	<option>--Couleur--</option>' ; ?>
		<?= '	<option value="#00FFFF">Turquoise</option>' ; ?>
		<?= '	<option value="#000000">Noir</option>' ; ?>
		<?= '	<option value="#0000FF">Bleu foncé</option>' ; ?>
		<?= '	<option value="#FF00FF">Rose foncé</option>' ; ?>
		<?= '	<option value="#808080">Gris foncé</option>' ; ?>
		<?= '	<option value="#008000">Vert foncé</option>' ; ?>
		<?= '	<option value="#00FF00">Vert clair</option>' ; ?>
		<?= '	<option value="#800000">Marron</option>' ; ?>
		<?= '	<option value="#800080">Violet</option>' ; ?>
		<?= '	<option value="#FF0000">Rouge</option>' ; ?>
		<?= '	<option value="#C0C0C0">Gris clair</option>' ; ?>
		<?= '	<option value="#FFFFFF">Blanc</option>' ; ?>
		<?= '	<option value="#FFFF00">Jaune</option>' ; ?>
		<?= '	<option value="#D2691E">Chocolat</option>' ; ?>
		<?= '	<option value="#FFA500">Orange clair</option>' ; ?>
		<?= '	<option value="#FF4500">Orange Rouge</option>' ; ?>
		<?= '</select>' ; ?>
		<?= '<input id="sub-btn" name = "SubB" type="submit">' ; ?>
	<?= '</form>
	<p style ="margin-left:20%;"><strong> Ne pas mettre une heure inférieure à 8 et une heure supérieur à 21. </strong></p></div>' ; ?>
	<?php }
	else {

	}
	?>
	
	<script type="text/javascript" src="assets/js/script.js"></script>
	
	<?php 
		$planning = json_decode(file_get_contents("assets/js/planning.json"), true);
		
		if(isset($_POST['SubB'])) {
			$day = $_POST['day'];
			$hours = $_POST['hours'];
			$text = $_POST['text'];
			$color = $_POST['color'];
			if(!empty($hours) ) {
				if(in_array($hours, $planning[$day]['hour'])) {
					$index = array_search($hours, $planning[$day]['hour']);
					if(isset($_POST['delete'])) {
						array_splice($planning[$day]['hour'], $index);
						array_splice($planning[$day]['text'], $index);
						array_splice($planning[$day]['color'], $index);	
					}
					elseif(!empty($text) && !empty($color)) {
						$planning[$day]['hour'][$index] = $hours;
						$planning[$day]['text'][$index] = $text;
						$planning[$day]['color'][$index] = $color;
					}
					
				}
				elseif(!empty($text) && !empty($color)) {
					array_push($planning[$day]['hour'], $hours);
					array_push($planning[$day]['text'], $text);
					array_push($planning[$day]['color'], $color);
				}

				$fp = fopen('assets/js/planning.json', 'w');
				fwrite($fp, json_encode($planning));
				fclose($fp);
			}
		}
		?>
		<script src="assets/js/jquery.min.js"></script>
		<script src="assets/bootstrap/js/bootstrap.min.js"></script>
		<script src="assets/js/bs-init.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
		<script src="assets/js/Simple-Slider.js"></script>
	<?php require_once('footer.php');	?>
