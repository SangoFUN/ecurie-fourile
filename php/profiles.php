<?php 
    if(session_status() == PHP_SESSION_NONE) {
       session_start();
       }
       if($_SESSION['type'] == 0) {
        header('Location: ./');
        exit();
    }

    ?>
    <!DOCTYPE html>
    <html lang="fr">
    
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
        <title>Liste des comptes</title>
        <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abril+Fatface">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alfa+Slab+One">
        <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora">
        <link rel="stylesheet" href="assets/fonts/ionicons.min.css">
        <link rel="stylesheet" href="assets/css/Article-Clean.css">
        <link rel="stylesheet" href="assets/css/Article-List.css">
        <link rel="stylesheet" href="assets/css/Contact-Form-Clean.css">
        <link rel="stylesheet" href="assets/css/Features-Boxed.css">
        <link rel="stylesheet" href="assets/css/Footer-Dark.css">
        <link rel="stylesheet" href="assets/css/Highlight-Blue.css">
        <link rel="stylesheet" href="assets/css/Highlight-Clean.css">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css">
        <link rel="stylesheet" href="assets/css/Login-Form-Clean.css">
        <link rel="stylesheet" href="assets/css/Login-Form-Dark.css">
        <link rel="stylesheet" href="assets/css/Projects-Horizontal.css">
        <link rel="stylesheet" href="assets/css/Registration-Form-with-Photo.css">
        <link rel="stylesheet" href="assets/css/Simple-Slider.css">
        <link rel="stylesheet" href="assets/css/Team-Boxed.css">
        <link rel="stylesheet" href="assets/css/Navigation-with-Button.css">
        <link rel="stylesheet" href="assets/css/style.css">
    </head>
    
    <body>
          
        
    
    <?php
    require_once('menu.php');
    require_once('BDD.php');
    $conn = new mysqli($db_server, $db_user_login, $db_user_pass, $db_name);
    
    if ($conn->connect_error) {
        die('<p style="color:red">'."Database connection failed: " . $conn->connect_error);
    }


    if (isset($_GET['id']))
    {
        $id = $_GET['id'];
        $sql = "SELECT * FROM account WHERE id='$id'";
        
        $result = $conn->query($sql);
        
        if ($result->num_rows > 0) {
            
            if($row = $result->fetch_assoc()) {
                echo '<div class="column content">';
                echo '<div class="col-md-12"></div>';
                echo '<div class="user_profile">';
                echo '<h1>Le profil de '.$row["first_name"]."</h1>";
                echo '</div>';
                echo '<table class="user2">';
                echo '<tr><td>ID:</td><td>'.$row["id"].'</td></tr>';
                echo '<tr><td>Prénom:</td><td>'.$row["first_name"].'</td></tr>';
                echo '<tr><td>Nom:</td><td>'.$row["last_name"].'</td></tr>';
                echo '<tr><td>Type de compte:</td><td>'.$row["type"].'</td></tr>';
                echo '<tr><td> <br>'  . '</td></tr>';
                echo '<tr><td><a class="btn btn-primary" href="change_account.php?id=' .$row["id"]. '"> Modifier le compte</a></td></tr>';
                echo '<tr><td> <br>'  . '</td></tr>';
                echo '<tr><td><a class="btn btn-primary" href="delete_account.php?id=' .$row["id"]. '"> Supprimer le compte</a></td></tr>';
                echo '</div>';
            }
            echo '</table>';
        }
        else {
           echo "0 results";
        }
    }
    else {

        echo '<h2 class="title_user content" style="margin-top:100px;">Tous les utilisateurs:</h2>';

        $sql = "SELECT * FROM account";
        
        $result = $conn->query($sql);
        
        if ($result->num_rows > 0) {
   
            while($row = $result->fetch_assoc()) {
                echo '<div class="row">';
                echo '<div class="col-md-12 ;"></div>';
                echo '<hr>';
                echo '<table class="user">';
                echo '<tr><td>ID:</td><td>'.$row["id"].'</td></tr>';
                echo '<tr><td>Prénom:</td><td>'.$row["first_name"].'</td></tr>';
                echo '<tr><td>Nom:</td><td>'.$row["last_name"].'</td></tr>';
                echo '<td><a class="btn btn-primary" href="profiles.php?id=' . $row["id"] . '"> Voir profil de l\'utilisateur </a></td></tr>';
                echo '</table>';
                echo '</div>';
                
            }
        }
        else {
           echo "0 results";
        }
    }
    ?> 
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="assets/js/bs-init.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
    <script src="assets/js/Simple-Slider.js"></script>

   <?php require_once('footer.php'); ?>
