<?php 
 if(session_status() == PHP_SESSION_NONE) {
    session_start();
    }
?>
<!DOCTYPE html>
<html lang="fr">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, shrink-to-fit=no">
    <title>Information sur l'activité</title>
    <link rel="stylesheet" href="assets/bootstrap/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Abril+Fatface">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Alfa+Slab+One">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lora">
    <link rel="stylesheet" href="assets/fonts/font-awesome.min.css">
    <link rel="stylesheet" href="assets/css/Article-Clean.css">
    <link rel="stylesheet" href="assets/css/Article-List.css">
    <link rel="stylesheet" href="assets/css/Contact-Form-Clean.css">
    <link rel="stylesheet" href="assets/css/Features-Boxed.css">
    <link rel="stylesheet" href="assets/css/Footer-Dark.css">
    <link rel="stylesheet" href="assets/css/Highlight-Blue.css">
    <link rel="stylesheet" href="assets/css/Highlight-Clean.css">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/css/swiper.min.css">
    <link rel="stylesheet" href="assets/css/Login-Form-Clean.css">
    <link rel="stylesheet" href="assets/css/Login-Form-Dark.css">
    <link rel="stylesheet" href="assets/css/Projects-Horizontal.css">
    <link rel="stylesheet" href="assets/css/Registration-Form-with-Photo.css">
    <link rel="stylesheet" href="assets/css/Simple-Slider.css">
    <link rel="stylesheet" href="assets/css/Team-Boxed.css">
    <link rel="stylesheet" href="assets/css/Navigation-with-Button.css">
    <link rel="stylesheet" href="assets/css/style.css">
</head>

<body>
    <?php require_once('menu.php'); ?>

<section class="article-list content">
<div class="container">
    <div class="intro">
<?php 
require_once('BDD.php');
        
        $conn = mysqli_connect($db_server,$db_user_login , $db_user_pass,$db_name);
        
        
        if (isset($_GET['id']))
    {
        $id = $_GET['id'];
        $req = "SELECT * FROM activity WHERE id='$id'";
        
        $res = mysqli_query($conn,$req);
 
        
        if ($res->num_rows > 0) {
            
            if($row = $res->fetch_assoc()) {
                $image = $row['name_img'];
                $image_src = "upload/".$image;
                ?>
                <?= '<h2 class="text-center">' . $row["title"] . '</h2>'; ?>
                <?= '<p class="text-center">' . $row['description_short'] . '</p>'; ?>
                <?= '</div>'; ?>
                <?= '<div class="container">' ?>
                <?= '<div class="row">' ?>
                <?= '<div class="col-md-12"><img src="upload/' ,   $row['name_img'] ,  '"  style="width: 100%;height: 100%;margin-top: 5%;margin-bottom: 5%;"></div>' ; ?>
                <?= '</div>'; ?>
                <?= '<div class="row">'; ?>
                <?= '<div class="col-md-12">'; ?>
                <?= '<section class="projects-clean">'; ?>
                        <?= '<div class="container">'; ?>
                            <?= '<div class="intro"></div>'; ?>
                            <?= '<div class="row projects">' ; ?>
                                <?= '<div class="col">' ?>
                                    <?= '<h1 style="margin-left: 40%;margin-top: 7%;"></h1>' ?>
                                    <?='<p style="margin-left: 0;margin-top: 5%;margin-bottom: 2%;">' , $row['description_long'] , '</p>';?>
                                    
                                    <?php if(isset($_SESSION['type'])) {
                                        if($_SESSION['type'] == 1) {
                                            echo '<a class="btn btn-primary center" style="margin-bottom:5%;margin-left: 30%;" href="change_activity_info?id=' .$row["id"]. '"> Modifier les informations</a>';
                                            echo '<a class="btn btn-primary center" style="margin-bottom:5%;margin-left: 10%;" href="delete_activity?id=' .$row["id"]. '"> Supprimer l\'activité </a>';
                                        }
                                    }
                                    else {
                                        
                                        }
                                        ?>
                                <?= '</div>'; ?>
                            <?= '</div>'; ?>
                        <?= '</div>' ; ?>
                <?php
                echo '</section>';
                echo '</section>';
                echo '</div>';
            }
        }
        else {
           echo "0 results";
        }
    }
    ?>
    <script src="assets/js/jquery.min.js"></script>
    <script src="assets/bootstrap/js/bootstrap.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/Swiper/3.3.1/js/swiper.jquery.min.js"></script>
    <script src="assets/js/Simple-Slider.js"></script>
<?php require_once('footer.php'); ?>