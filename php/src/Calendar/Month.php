<?php

namespace Calendar;
class Month {

    public $days = ['Lundi', 'Mardi', 'Mercredi', 'Jeudi', 'Vendredi', 'Samedi', 'Dimanche'];

    private $months = ['Janvier', 'Février', 'Mars', 'Avril', 'Mai', 'Juin', 'Juillet', 'Aout', 'Septembre', 'Octobre', 'Novembre', 'Décembre'];
    public $month;
    public $year;
    public function __construct(?int $month = null, ?int $year= null) {
        if($month === null) {
            $month = intval(date('m'));
        }
        if($year === null) {
            $year = intval(date('Y'));
        }
        if($month < 1 || $month > 12) {
            throw new \Exception("Le mois $month n'est pas valide");
        }
        if($year < 1970) {
            throw new \Exception("L'année est inférieur à 1970");
        }
        $this->month = $month;
        $this->year = $year;
    }
    /* ici la fonction getStartingDay va nous permettre de renvoyer au premier jour du mois */
    public function getStartingDay (): \DateTime {
            return new \DateTime("{$this->year}-{$this->month}-01");
    }
    /* donc ici la fonction toString va retourner le mois en toute lettre comme par exemple Mai 2021 */
    public function toString(): string {
        return $this->months[$this->month - 1] . ' ' . $this->year;
    }
    public function getWeeks (): int {
        $start = $this->getStartingDay();
        $end = (clone $start)->modify('+1 month -1 day');
        $startWeek = intval($start->format('W'));
        $endWeek = intval($end->format('W'));
        if($endWeek === 1) {
            $endWeek = intval((clone $end)->modify('-7 days')->format('W')) + 1;
        }
        $weeks = $endWeek - $startWeek + 1;
        
        if($weeks < 0) {
            $weeks = intval($end->format('W'));
        }
    return ($weeks);
    }
    public function withinMonth(\DateTime $date): bool {
        return $this->getStartingDay()->format('Y-m') === $date->format('Y-m');
    }
    /* la fonction nextMonth permet de passer au mois suivant */
    public function nextMonth (): Month {
        $month = $this->month +1;
        $year = $this->year;
        if($month > 12) {
            $month = 1;
            $year += 1;
        }
        return new Month($month,$year);

    }
    /* la fonction previousMonth permet de passer au moins précédent */
    public function previousMonth (): Month {
        $month = $this->month -1;
        $year = $this->year;
        if($month < 1) {
            $month = 12;
            $year -= 1;
        }
        return new Month($month,$year);

    }
}